<?php require_once './code.php'?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>A01 - PHP Basics and Selection Control Structures</title>
</head>
<body>
    <h1>Full Address</h1>
    <p><?php echo getFullAddress('Philippines', 'Makati', 'Metro Manila', 'East Rembo');?></p>

    <h1>Letter-Based Grading</h1>
    <p><?php echo getLetterGrade(101);?></p>
</body>
</html>