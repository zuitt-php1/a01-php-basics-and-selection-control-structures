<?php 

function getFullAddress($country, $city, $province, $specificAddress){
    return "$specificAddress, $city, $province, $country";
};

function getLetterGrade($grade){
    if ($grade < 0){
        return "$grade is out of bounds";
    }
    elseif ($grade < 75){
        return "$grade is equivalent to D";
    }
    elseif ($grade === 75 || $grade === 76 ){
        return "$grade is equivalent to C-";
    }
    elseif ($grade >= 77 && $grade <= 79 ){
        return "$grade is equivalent to C";
    }
    elseif ($grade >= 80 && $grade <= 82 ){
        return "$grade is equivalent to C+";
    }
    elseif ($grade >= 83 && $grade <= 85 ){
        return "$grade is equivalent to B-";
    }
    elseif ($grade >= 86 && $grade <= 88 ){
        return "$grade is equivalent to B";
    }
    elseif ($grade >= 89 && $grade <= 91 ){
        return "$grade is equivalent to B+";
    }
    elseif ($grade >= 92 && $grade <= 94 ){
        return "$grade is equivalent to A-";
    }
    elseif ($grade >= 95 && $grade <= 97 ){
        return "$grade is equivalent to A";
    }
    elseif ($grade >= 98 && $grade <= 100 ){
        return "$grade is equivalent to A+";
    }
    else{
        return "$grade is out of bounds";
    }
}